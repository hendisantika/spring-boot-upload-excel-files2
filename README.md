# Spring Boot Upload Excel File

### Things todo list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-upload-excel-files2.git`
2. Go inside the folder: `cd spring-boot-upload-excel-files2`
3. Make sure your MySQL DB is running.
4. Run the application: `mvn clean spring-boot:run`
5. Open your favorite browser: http://localhost:8080

### Images Screen shot

Upload Page

![Upload Page](img/upload.png "Upload Page")

Success Page

![Success Page](img/success.png "Success Page")