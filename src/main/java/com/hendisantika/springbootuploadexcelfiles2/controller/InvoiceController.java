package com.hendisantika.springbootuploadexcelfiles2.controller;

import com.hendisantika.springbootuploadexcelfiles2.entity.Invoice;
import com.hendisantika.springbootuploadexcelfiles2.repository.InvoiceRepository;
import com.hendisantika.springbootuploadexcelfiles2.service.ExcelDataService;
import com.hendisantika.springbootuploadexcelfiles2.service.FileUploaderService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-upload-excel-files2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/02/21
 * Time: 07.29
 */
@Controller
@Log4j2
public class InvoiceController {

    @Autowired
    FileUploaderService fileService;

    @Autowired
    ExcelDataService excelservice;

    @Autowired
    InvoiceRepository repo;

    @GetMapping("/")
    public String index() {
        return "uploadPage";
    }

    @PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {

        fileService.uploadFile(file);

        redirectAttributes.addFlashAttribute("message",
                "You have successfully uploaded '" + file.getOriginalFilename() + "' !");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "redirect:/";
    }

    @GetMapping("/saveData")
    public String saveExcelData(Model model) {
        List<Invoice> excelDataAsList = excelservice.getExcelDataAsList();
        int noOfRecords = excelservice.saveExcelData(excelDataAsList);
        model.addAttribute("noOfRecords", noOfRecords);
        log.info("noOfRecords " + noOfRecords);
        return "success";
    }
}
