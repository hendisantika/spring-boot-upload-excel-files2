package com.hendisantika.springbootuploadexcelfiles2.service;

import com.hendisantika.springbootuploadexcelfiles2.entity.Invoice;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-upload-excel-files2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/02/21
 * Time: 22.18
 */
public interface ExcelDataService {
    List<Invoice> getExcelDataAsList();

    int saveExcelData(List<Invoice> invoices);
}
