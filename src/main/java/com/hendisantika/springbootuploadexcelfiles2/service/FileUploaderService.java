package com.hendisantika.springbootuploadexcelfiles2.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-upload-excel-files2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/02/21
 * Time: 22.18
 */
public interface FileUploaderService {
    void uploadFile(MultipartFile file);
}
