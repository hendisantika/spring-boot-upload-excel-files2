package com.hendisantika.springbootuploadexcelfiles2.repository;

import com.hendisantika.springbootuploadexcelfiles2.entity.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-upload-excel-files2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/02/21
 * Time: 22.17
 */
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

}