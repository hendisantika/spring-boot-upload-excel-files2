package com.hendisantika.springbootuploadexcelfiles2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootUploadExcelFiles2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootUploadExcelFiles2Application.class, args);
    }

}
